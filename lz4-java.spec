%global debug_package %nil
Name:                lz4-java
Version:             1.8.0
Release:             2
Summary:             LZ4 compression for Java
License:             Apache-2.0
URL:                 https://github.com/jpountz/lz4-java
Source0:             https://github.com/jpountz/lz4-java/archive/%{version}.tar.gz
Patch1:              1-remove-comments-from-templates.patch
Patch2:              2-remove-cpptasks.patch
BuildRequires:       java-devel >= 1:11.0.0
BuildRequires:       ant ant-junit aqute-bnd cpptasks ivy-local javapackages-local mvel gcc xxhash-devel
BuildRequires:       objectweb-asm randomizedtesting-junit4-ant bea-stax-api xerces-j2 apache-parent lz4 lz4-devel
Requires:            javapackages-tools

Provides:            bundled(lz4) = r122
Provides:            bundled(libxxhash) = r37
%description
LZ4 compression for Java, based on Yann Collet's work.
This library provides access to two compression methods
that both generate a valid LZ4 stream:
* fast scan (LZ4):
    ° low memory footprint (~ 16 KB),
    ° very fast (fast scan with skipping heuristics in case the
      input looks incompressible),
    ° reasonable compression ratio (depending on the
      redundancy of the input).
* high compression (LZ4 HC):
    ° medium memory footprint (~ 256 KB),
    ° rather slow (~ 10 times slower than LZ4),
    ° good compression ratio (depending on the size and
      the redundancy of the input).
The streams produced by those 2 compression algorithms use the
same compression format, are very fast to decompress and can be
decompressed by the same decompressor instance.

%package javadoc
Summary:             Javadoc for %{name}
BuildArch:           noarch
%description javadoc
This package contains javadoc for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1
find -name '*.dylib' -print -delete
find -name '*.so' -print -delete

%build
pushd /usr/lib/jvm
dirname=`ls | grep java-11-openjdk-`
javapath=/usr/lib/jvm/$dirname
popd
export JAVA_HOME=$javapath
export LIB_DIR=/usr/lib64/
sed -i '$a\	strip $(BUILD_DIR)/jni/net/jpountz/util/$(PLATFORM)/$(ARCH)/liblz4-java.so' Makefile
ant -Divy.mode=local -Divy.revision=%{version} -Divy.pom.version=%{version} jar test docs makepom
bnd wrap -p lz4-java.bnd -o dist/lz4-java-%{version}.jar --version %{version} dist/lz4-java.jar

%install
%mvn_artifact dist/lz4-java-%{version}.pom dist/lz4-java-%{version}.jar
%mvn_install -J build/docs

%files -f .mfiles
%doc CHANGES.md README.md
%license LICENSE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt

%changelog
* Thu Feb 22 2024 zke_012020 <keer.oerv@isrc.iscas.ac.cn> - 1.8.0-2
- Upgrade JDK version to 11

* Thu Jul 27 2023 chenchen <chen_aka_jan@163.com> - 1.8.0-1
- Upgrade version to 1.8.0

* Fri Mar 3 2023 caodongxia <caodongxia@h-partners.com> - 1.7.1-3
- Fix not stripped problem

* Tue May 10 2022 wangkai <wangkai385@h-partners.com> - 1.7.1-2
- License compliance rectification

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.7.1-1
- Upgrade to version 1.7.1

* Wed Jun 09 2021 wulei <wulei80@huawei.com> - 1.3.0-2
- fixes failed: Could not launch gcc

* Tue Aug 25 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.3.0-1
- Package init
